# Wayfarer #
Wayfarer is a journey about cooperation, trust and enlightenment or a good ol' PVP you decide!

Guide your player through procedurally generated levels, avoiding obstacles and the fall! Pray to the gods at the midway point for a safe passage and successful exit. To add to the challenge, team up with a friend and play the level blindfolded while receiving instructions from your team mate!

Note: Requires 2 Game controllers to play! (for now). The game uses Cocos2d-x(3.x) and SDL2

## Compiling - Windows ##
1. Clone repo
2. Create a cocos2d-x project and copy over the Classes and Resources folders
3. Open the .sln file in proj.win32
4. remove existing files from Classes and add the new ones
5. Add references to SDL2 include folders and SDL2.lib in project settings
6. Compile!