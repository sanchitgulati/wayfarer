#ifndef __GameScene_SCENE_H__
#define __GameScene_SCENE_H__

#include "cocos2d.h"
#include "GameController.h"

#define MAP_WIDTH 25
#define MAP_HEIGHT 15

#define PLAYER_MAX 10

class GameScene : public cocos2d::Layer
{
    int _map[MAP_HEIGHT][MAP_WIDTH];
    cocos2d::Vector<cocos2d::Sprite*> _platformsCollision;
    cocos2d::Vector<cocos2d::Sprite*> _obstacleCollision;
    cocos2d::Vector<cocos2d::Sprite*> _idolCollision;
    cocos2d::Sprite* _playerOne;
    cocos2d::Sprite* _playerTwo;
    cocos2d::Sprite* _playerOneWin;
    cocos2d::Sprite* _playerTwoWin;
    cocos2d::Label* _playerOnePattern;
	cocos2d::Sprite* bloom7;
	cocos2d::Sprite* bloom9;
    int _playerOnePatternTest[10];
    int _playerTwoPatternTest[10];
    int _playerOnePatternTestCount;
    int _playerTwoPatternTestCount;
    cocos2d::Label* _playerTwoPattern;
    GameController* _gcHandler;
    cocos2d::Vec2 _playerOneMovement;
    cocos2d::Vec2 _playerTwoMovement;
    cocos2d::Vec2 _playerOneSpawn;
    cocos2d::Vec2 _playerTwoSpawn;
	cocos2d::Vec2 _playerReSpawn;
	cocos2d::DrawNode* _playerOneCollisionRect;
	cocos2d::DrawNode* _playerTwoCollisionRect;
	cocos2d::ParticleSystemQuad* _bloom7particle_sun;
	cocos2d::ParticleSystemQuad* _bloom7particle_tail;
	cocos2d::ParticleSystemQuad* _bloom9particle_sun;
	cocos2d::ParticleSystemQuad* _bloom9particle_tail;
private:
    void mapGenerator();
	void restartGame();
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    void update(float dt);
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(GameScene);
    void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
};

#endif // __GameScene_SCENE_H__
