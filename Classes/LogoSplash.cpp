//
//  ;
//  Adam
//
//  Created by Sanchit Gulati on 02/08/14.
//
//

#include "LogoSplash.h"
#include "GameScene.h"

using namespace cocos2d;


Scene* LogoSplash::createScene()
{
    auto scene = Scene::create();
    auto layer = LogoSplash::create();
    //layer->setTag(0);
    scene->addChild(layer);
    return scene;
}


LogoSplash::LogoSplash() {
    
}

LogoSplash::~LogoSplash() {
    
}

bool LogoSplash::init() {
    Layer::init();
    Size screenSize = Director::getInstance()->getVisibleSize();
    
    
    auto bg = Sprite::create("images/ggj.png");
    bg->setScale(screenSize.height/bg->getContentSize().height);
    bg->setPosition(screenSize.width/2,screenSize.height/2);
    addChild(bg);
    
    
    auto bg2 = Sprite::create("images/ggjblr.png");
    bg2->setScale(screenSize.height/bg2->getContentSize().height);
    bg2->setPosition(screenSize.width/2,screenSize.height/2);
    bg2->setOpacity(0);
    addChild(bg2);
    
//    auto cf = CallFunc::create([this](){
//        Director::getInstance()->replaceScene(GameScene::createScene());
//    });
    
    
    auto bg3 = Sprite::create("images/start.png");
    bg3->setScale(screenSize.height/bg2->getContentSize().height);
    bg3->setPosition(screenSize.width/2,screenSize.height/2);
    bg3->setOpacity(0);
    addChild(bg3);

	auto bg4 = Sprite::create("images/ggjPlay.png");
	bg4->setScale(screenSize.height / bg3->getContentSize().height);
	bg4->setPosition(screenSize.width / 2, screenSize.height / 2);
	bg4->setOpacity(0);
	addChild(bg4);
    
    
    bg->runAction(Sequence::create(DelayTime::create(1),FadeOut::create(1), NULL));
    bg2->runAction(Sequence::create(DelayTime::create(2),FadeIn::create(1), NULL));
    bg3->runAction(Sequence::create(DelayTime::create(6),FadeIn::create(1), NULL));
	bg4->runAction(Sequence::create(DelayTime::create(10), FadeIn::create(1), NULL));
//    runAction(Sequence::create(DelayTime::create(4),cf, NULL));
    
    
    _gcHandler = GameController::create();
    addChild(_gcHandler);
    
	//Initializing and binding
	auto listener = EventListenerKeyboard::create();
	listener->onKeyPressed = CC_CALLBACK_2(LogoSplash::onKeyPressed, this);
	listener->onKeyReleased = CC_CALLBACK_2(LogoSplash::onKeyReleased, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    scheduleUpdate();
    return true;
}



void LogoSplash::onEnter() {
    
    Layer::onEnter();
}


void LogoSplash::update(float dt){
    if(_gcHandler->isTwoABXYPressed()[0] || _gcHandler->isTwoABXYPressed()[1] || _gcHandler->isTwoABXYPressed()[2] || _gcHandler->isTwoABXYPressed()[3] ){
        Director::getInstance()->replaceScene(TransitionProgressInOut::create(1,GameScene::createScene()));
    }
    if(_gcHandler->isOneABXYPressed()[0] || _gcHandler->isOneABXYPressed()[1] || _gcHandler->isOneABXYPressed()[2] || _gcHandler->isOneABXYPressed()[3] ){
        Director::getInstance()->replaceScene(TransitionProgressInOut::create(1,GameScene::createScene()));
    }
    
}


// a selector callback
void LogoSplash::menuCallback(cocos2d::Ref* pSender)
{
    
}

void LogoSplash::onExit() {
    Layer::onExit();
}


// Implementation of the keyboard event callback function prototype	
void LogoSplash::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
	log("Key with keycode %d pressed", keyCode);
	if (keyCode == EventKeyboard::KeyCode::KEY_ESCAPE) {
		Director::getInstance()->end();
	}
}


void LogoSplash::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{

}