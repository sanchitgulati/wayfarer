//
//  Controller.cpp
//  litt
//
//  Created by Sanchit Gulati on 01/12/15.
//
//

#include "GameController.h"


USING_NS_CC;
const int JOYSTICK_DEAD_ZONE = 8000;

extern SDL_Haptic *haptic1,*haptic2;
bool GameController::init(){
    
    xDir1 = 0;
    yDir1 = 0;
    xDir2 = 0;
    yDir2 = 0;
    btnA1 = false;
    btnA2 = false;
    btnB1 = false;
    btnB2 = false;
    btnX1 = false;
    btnX2 = false;
    btnY1 = false;
    btnY2 = false;
    
    _lastFive1.push_back(0);
    _lastFive1.push_back(0);
    _lastFive1.push_back(0);
    _lastFive1.push_back(0);
    _lastFive1.push_back(0);
    
    
    _lastFive2.push_back(0);
    _lastFive2.push_back(0);
    _lastFive2.push_back(0);
    _lastFive2.push_back(0);
    _lastFive2.push_back(0);
    
    
    auto listener = EventListenerKeyboard::create();
    listener->onKeyPressed = CC_CALLBACK_2(GameController::onKeyPressed, this);
    listener->onKeyReleased = CC_CALLBACK_2(GameController::onKeyReleased, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    if(!registerControllerListener())
        registerControllerListenerSDL();
    return true;
}

void GameController::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
    
    switch (keyCode) {
        case EventKeyboard::KeyCode::KEY_UP_ARROW:
            break;
            
        case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
            break;
            
        case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
            break;
        default:
            break;
    }
    
}

void GameController::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
    switch (keyCode) {
        case EventKeyboard::KeyCode::KEY_UP_ARROW:
            break;
            
        case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
            break;
            
        case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
            break;
        default:
            break;
    }
}



bool GameController::registerControllerListener()
{
    return false; //stopping cocos2d-x discovery of controller for now.
    
    //auto listener = EventListenerController::create();
    //listener->onConnected = CC_CALLBACK_2(GameController::onConnectController,this);
    //listener->onDisconnected = CC_CALLBACK_2(GameController::onDisconnectedController,this);
    //listener->onKeyDown = CC_CALLBACK_3(GameController::onKeyDown, this);
    //listener->onKeyUp = CC_CALLBACK_3(GameController::onKeyUp, this);
    //listener->onAxisEvent = CC_CALLBACK_3(GameController::onAxisEvent, this);
    //_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    //This function should be called for iOS platform
    //    Controller::startDiscoveryController();
    
}

bool GameController::registerControllerListenerSDL(){
    //Check for joysticks
    if( SDL_NumJoysticks() < 1 )
    {
        log( "Warning: No joysticks connected!\n" );
    }
    else
    {
        scheduleUpdate();
        return true;
    }
    return false;
}


Vec2 GameController::oneAxis(){
    return Vec2(xDir1,yDir1);
}
Vec2 GameController::twoAxis(){
    return Vec2(xDir2,yDir2);
}


std::vector<int> GameController::isOneABXYPressed(){
    std::vector<int> ret;
    ret.push_back(btnX1);
    ret.push_back(btnY1);
    ret.push_back(btnA1);
    ret.push_back(btnB1);
    ret.push_back(btnRR1);
    return ret;
}


std::vector<int> GameController::isTwoABXYPressed(){
    std::vector<int> ret;
    ret.push_back(btnX2);
    ret.push_back(btnY2);
    ret.push_back(btnA2);
    ret.push_back(btnB2);
    ret.push_back(btnRR2);
    return ret;
}


void GameController::vibrate1(){
    if(haptic1 != NULL){
        SDL_HapticRumblePlay( haptic2, 0.5, 2000 );
    }
}

void GameController::vibrate2(){
    if(haptic2 != NULL){
        SDL_HapticRumblePlay( haptic1, 0.5, 2000 );
    }
}



void GameController::update(float dt){
    //Handle events on queue
    while( SDL_PollEvent( &e ) != 0 )
    {
        //User requests quit
        if( e.type == SDL_QUIT )
        {
            quit = true;
        }
        else if( e.type == SDL_JOYAXISMOTION )
        {
            //Motion on controller 0
            if( e.jaxis.which == 0 )
            {
                //X axis motion
                if( e.jaxis.axis == 0 )
                {
                    //Left of dead zone
                    if( e.jaxis.value < -JOYSTICK_DEAD_ZONE ){xDir1 = -1;}
                    //Right of dead zone
                    else if( e.jaxis.value > JOYSTICK_DEAD_ZONE ){xDir1 =  1;}
                    else {xDir1 = 0;}
                }
                //Y axis motion
                else if( e.jaxis.axis == 1 )
                {
                    //Below of dead zone
                    if( e.jaxis.value < -JOYSTICK_DEAD_ZONE ){yDir1 = -1;}
                    //Above of dead zone
                    else if( e.jaxis.value > JOYSTICK_DEAD_ZONE ){yDir1 =  1;}
                    else{yDir1 = 0;}
                }
                
                
            }
            else if(e.jaxis.which == 1){
                //X axis motion
                if( e.jaxis.axis == 0 )
                {
                    //Left of dead zone
                    if( e.jaxis.value < -JOYSTICK_DEAD_ZONE ){xDir2 = -1;}
                    //Right of dead zone
                    else if( e.jaxis.value > JOYSTICK_DEAD_ZONE ){xDir2 =  1;}
                    else {xDir2 = 0;}
                }
                //Y axis motion
                else if( e.jaxis.axis == 1 )
                {
                    //Below of dead zone
                    if( e.jaxis.value < -JOYSTICK_DEAD_ZONE ){yDir2 = -1;}
                    //Above of dead zone
                    else if( e.jaxis.value > JOYSTICK_DEAD_ZONE ){yDir2 =  1;}
                    else{yDir2 = 0;}
                }
            }
        }
        
        if(e.type == SDL_JOYBUTTONDOWN){
            
            //Motion on controller 0
            if( e.jaxis.which == 0 )
            {
                if ( e.jbutton.button == 0 ) //X key
                {
                    btnA1 = true;
                }
                if ( e.jbutton.button == 1 ) //? key
                {
                    btnB1 = true;
                }
                if ( e.jbutton.button == 2 ) //? key
                {
                    btnX1 = true;
                }
                if ( e.jbutton.button == 3 ) //? key
                {
                    btnY1 = true;
                }
                if ( e.jbutton.button == 4 ) //? key
                {
                    btnRR1 = true;
                }
            }
            else if( e.jaxis.which == 1 )
            {
                if ( e.jbutton.button == 0 ) //X key
                {
                    btnA2 = true;
                }
                if ( e.jbutton.button == 1 ) //? key
                {
                    btnB2 = true;
                }
                if ( e.jbutton.button == 2 ) //? key
                {
                    btnX2 = true;
                }
                if ( e.jbutton.button == 3 ) //? key
                {
                    btnY2 = true;
                }
                if ( e.jbutton.button == 4 ) //? key
                {
                    btnRR2 = true;
                }
            }
        }
        if(e.type == SDL_JOYBUTTONUP){
            
            //Motion on controller 0
            if( e.jaxis.which == 0 )
            {
                if ( e.jbutton.button == 0 ) //X key
                {
                    btnA1 = false;
                }
                if ( e.jbutton.button == 1 ) //Y key
                {
                    btnB1 = false;
                }
                if ( e.jbutton.button == 2 ) //A key
                {
                    btnX1 = false;
                }
                if ( e.jbutton.button == 3 ) //B key
                {
                    btnY1 = false;
                }
                if ( e.jbutton.button == 4 ) //? key
                {
                    btnRR1 = false;
                }
                _lastFive1.push_back(e.jbutton.button);
                _lastFive1.pop_front();
            }
            else if( e.jaxis.which == 1 )
            {
                if ( e.jbutton.button == 0 ) //X key
                {
                    btnA2 = false;
                }
                if ( e.jbutton.button == 1 ) //Y key
                {
                    btnB2 = false;
                }
                if ( e.jbutton.button == 2 ) //A key
                {
                    btnX2 = false;
                }
                if ( e.jbutton.button == 3 ) //B key
                {
                    btnY2 = false;
                }
                if ( e.jbutton.button == 4 ) //? key
                {
                    btnRR2 = false;
                }
                _lastFive2.push_back(e.jbutton.button);
                _lastFive2.pop_front();
            }
        }
    }
    
    //    log("xDir %d yDir %d",xDir,yDir);
}



std::deque<int> GameController::last5One(){
    return _lastFive1;
}

std::deque<int> GameController::last5Two(){
    return _lastFive2;
}

////Controller is the obejects of the Controller，keyCode means the keycode of the controller you click down
void GameController::onKeyDown(Controller *controller, int keyCode, cocos2d::Event *event)
{
    CCLOG("KeyDown:%d", keyCode);
}

void GameController::onKeyUp(Controller *controller, int keyCode, cocos2d::Event *event)
{
    //You can get the controller by tag, deviceId or devicename if there are multiple controllers
    CCLOG("tag:%d DeviceId:%d DeviceName:%s", controller->getTag(), controller->getDeviceId(), controller->getDeviceName().c_str());
    CCLOG("KeyUp:%d", keyCode);
}

//The axis includes X-axis and Y-axis and its range is from -1 to 1. X-axis is start from left to right and Y-axis is bottom to top.
void GameController::onAxisEvent(Controller* controller, int keyCode, cocos2d::Event* event)
{
    //    const auto& keyStatus = controller->getKeyStatus(keyCode);
    //    CCLOG("Axis KeyCode:%d Axis Value:%f", keyCode, keyStatus.value);
}

void GameController::onConnectController(Controller* controller, Event* event)
{
    CCLOG("Game controller connected");
}

void GameController::onDisconnectedController(Controller* controller, Event* event)
{
    CCLOG("Game controller disconnected");
}

void GameController::releaseControllers(){
    
}

void GameController::onExit(){
    Node::onExit();
}
