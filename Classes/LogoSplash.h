#ifndef __LogoSplash_SCENE_H__
#define __LogoSplash_SCENE_H__

#include "cocos2d.h"
#include "GameController.h"

class LogoSplash : public cocos2d::Layer
{
private:
    GameController* _gcHandler;
public:
    LogoSplash();
    ~LogoSplash();
    virtual bool init();
    void onEnter();
    void onExit();
    void update(float dt);
    // a selector callback
    void menuCallback(cocos2d::Ref* pSender);
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();
    CREATE_FUNC(LogoSplash);
	void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
	void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
};

#endif