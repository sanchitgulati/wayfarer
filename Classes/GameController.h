//
//  Controller.hpp
//  litt
//
//  Created by Sanchit Gulati on 01/12/15.
//
//

#ifndef Controller_hpp
#define Controller_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "base/CCGameController.h"
#include <SDL2/SDL.h>


class GameController :public cocos2d::Node
{
private:
    void onExit() override;
    
    //Main loop flag
    bool quit = false;
    //Event handler
    SDL_Event e;
    
    //Normalized direction
    int xDir1;
    int yDir1;
    int xDir2;
    int yDir2;
    bool btnA1;
    bool btnA2;
    bool btnB1;
    bool btnB2;
    bool btnX1;
    bool btnX2;
    bool btnY1;
    bool btnY2;
    bool btnRR1;
    bool btnRR2;
    std::deque<int> _lastFive1;
    std::deque<int> _lastFive2;
protected:
    //Game Controller 1 handler
    void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
    bool registerControllerListener();
    bool registerControllerListenerSDL();
    
    void onConnectController(cocos2d::Controller* controller, cocos2d::Event* event);
    void onDisconnectedController(cocos2d::Controller* controller, cocos2d::Event* event);
    void onKeyDown(cocos2d::Controller *controller, int keyCode, cocos2d::Event *event);
    void onKeyUp(cocos2d::Controller *controller, int keyCode, cocos2d::Event *event);
    void onAxisEvent(cocos2d::Controller* controller, int keyCode, cocos2d::Event* event);
public:
    CREATE_FUNC(GameController);
    bool init() override;
    void update(float dt) override;
    
    cocos2d::Vec2 oneAxis();
    std::vector<int> isOneABXYPressed();
    std::deque<int> last5One();
    
    void vibrate1();
    void vibrate2();
    
    
    cocos2d::Vec2 twoAxis();
    std::vector<int> isTwoABXYPressed();
    std::deque<int> last5Two();
    
    void releaseControllers();
};

#endif /* Controller_hpp */
