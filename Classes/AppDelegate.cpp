#include "AppDelegate.h"
#include "LogoSplash.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

SDL_Joystick* gGameController1 = nullptr;
SDL_Joystick* gGameController2 = nullptr;
SDL_Haptic *haptic1 = nullptr;
SDL_Haptic *haptic2 = nullptr;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}

// If you want to use packages manager to install more packages, 
// don't modify or remove this function
static int register_all_packages()
{
    return 0; //flag for packages manager
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
//        glview = GLViewImpl::create("My Game");
        glview = GLViewImpl::createWithFullScreen("My Game");
        director->setOpenGLView(glview);
    }

    // turn on display FPS
    director->setDisplayStats(false);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);

    register_all_packages();
    
    
    //Initialize SDL
    if( SDL_Init(SDL_INIT_JOYSTICK | SDL_INIT_HAPTIC) < 0 )
    {
        log( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
    }
    //Load joystick
    gGameController1 = SDL_JoystickOpen( 0 );
    if( gGameController1 == NULL )
    {
        log( "Warning: Unable to open game controller! SDL Error: %s\n", SDL_GetError() );
    }
    //Load joystick
    gGameController2 = SDL_JoystickOpen( 1 );
    if( gGameController2 == NULL )
    {
        log( "Warning: Unable to open game controller! SDL Error: %s\n", SDL_GetError() );
    }
    
    
    haptic1 = SDL_HapticOpenFromJoystick(gGameController1);
    haptic2 = SDL_HapticOpenFromJoystick( gGameController2);
    
    if(haptic1 != NULL){
        SDL_HapticRumbleInit( haptic1 );
		log("not null!! for 1");
    }
    if(haptic2 != NULL){
        SDL_HapticRumbleInit( haptic2 );
		log("not null!! for 2");
    }

    
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic("images/bg.wav");
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("images/bg.wav",true);

    // create a scene. it's an autorelease object
    auto scene = LogoSplash::createScene();

    // run
    director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    
    
    SDL_JoystickClose(gGameController1);
    SDL_JoystickClose(gGameController2);
    SDL_HapticClose( haptic1 );
    SDL_HapticClose( haptic2 );
    gGameController1 = nullptr;
    gGameController2 = nullptr;
    //Close game controller
    SDL_Quit();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
