#include "GameScene.h"


USING_NS_CC;
#define PIXEL 100

Scene* GameScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = GameScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    
    //08,08,14
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    
    _playerOnePatternTestCount = 0;
    _playerTwoPatternTestCount = 0;
    
    auto bg = Sprite::create("images/bg.png");
    bg->setScale( visibleSize.width/bg->getContentSize().width, visibleSize.width / bg->getContentSize().width); ///  visibleSize.height/bg->getContentSize().height
	bg->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
	bg->setPosition(0, visibleSize.height); /// visibleSize.width/2,visibleSize.height/2
    //bg->setColor(Color3B(8,8,14));
    addChild(bg);
    
    std::stringstream pattern1;
    for (int i = 0 ; i < 5; i++) {
        auto rand = RandomHelper::random_int(0, 3);
        _playerOnePatternTest[i] = rand;
        switch (rand) {
            case 0:
                pattern1 << "a";
                break;
            case 1:
                pattern1 << "b";
                break;
            case 2:
                pattern1 << "x";
                break;
            case 3:
                pattern1 << "y";
                break;
            default:
                break;
        }
        if(i != 4)
            pattern1 << "+";
    }
    
    _playerOnePattern = Label::createWithTTF(pattern1.str().substr(), "fonts/Phosphate-Inline.ttf", 48);
    _playerOnePattern->setPosition(visibleSize.width- 30,30);
    _playerOnePattern->setColor(Color3B(178,0,72));
    _playerOnePattern->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
    addChild(_playerOnePattern,3029);
    
    std::stringstream pattern2;
    for (int i = 0 ; i < 5; i++) {
        auto rand = RandomHelper::random_int(0, 3);
        _playerTwoPatternTest[i] = rand;
        switch (rand) {
            case 0:
                pattern2 << "a";
                break;
            case 1:
                pattern2 << "b";
                break;
            case 2:
                pattern2 << "x";
                break;
            case 3:
                pattern2 << "y";
                break;
            default:
                break;
        }
        if(i != 4)
            pattern2 << "+";
    }
    
    
    _playerTwoPattern = Label::createWithTTF(pattern2.str().c_str(), "fonts/Phosphate-Inline.ttf", 48);
    _playerTwoPattern->setPosition(visibleSize.width- 30,visibleSize.height - 30);
    _playerTwoPattern->setColor(Color3B(51,94,255));
    _playerTwoPattern->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
    addChild(_playerTwoPattern,3029);
    
    
    mapGenerator();
    
    
    
    _gcHandler = GameController::create();
    addChild(_gcHandler);
    
    auto parent = Node::create();
    addChild(parent);
    
    auto platform = Node::create();
    platform->setContentSize(Size(MAP_WIDTH*PIXEL,MAP_HEIGHT*PIXEL));
    addChild(platform);
    int pillar = 0;
    for (int i = MAP_HEIGHT-1; i >= 0; i--) {
        for(int j = 0; j < MAP_WIDTH;j++){
            int val = _map[i][j];
            
            switch (val) {
                case 0:{ /// regular platform 
                    pillar++;
                    auto sprite = Sprite::create("images/0.png");
                    sprite->setPosition(j*PIXEL,i*PIXEL);
                    platform->addChild(sprite,10);
                    _platformsCollision.pushBack(sprite);
                    if(pillar == 10){
                        pillar = 0;
                        auto s = Sprite::create("images/0_bottom_alt.png");
                        s->setPosition(s->getContentSize().width/2,-1*s->getContentSize().height/2);
                        sprite->addChild(s,2);
                        
                        auto p = Sprite::create("images/pillar.png");
                        p->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
                        p->setPosition(s->getContentSize().width/2,-1*s->getContentSize().height/2);
                        sprite->addChild(p);
                    }
                    else{
                        auto s = Sprite::create("images/0_bottom.png");
                        s->setPosition(s->getContentSize().width/2,-1*s->getContentSize().height/2);
                        sprite->addChild(s,2);
                    }
                    break;
                }
                case 7:{ /// player end point 
                    auto sprite = Sprite::create("images/7.png");
                    sprite->setPosition(j*PIXEL,i*PIXEL);
                    platform->addChild(sprite,10);
                    
					auto s = Sprite::create("images/0_bottom.png");
                    s->setPosition(s->getContentSize().width/2,-1*s->getContentSize().height/2);
                    sprite->addChild(s);
					
					bloom7 = Sprite::create("images/end_bloom.png");
					bloom7->setPosition(bloom7->getContentSize().width/5, 0);
					bloom7->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
					sprite->addChild(bloom7);
					bloom7->setVisible(false);

					_bloom7particle_sun = cocos2d::ParticleSystemQuad::create("images/bloom_sun_particle.plist");
					_bloom7particle_sun->setPosition(bloom7->getContentSize().width / 5, 42);
					_bloom7particle_sun->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
					sprite->addChild(_bloom7particle_sun);
					_bloom7particle_sun->setVisible(false);

					_bloom7particle_tail = cocos2d::ParticleSystemQuad::create("images/bloom_tail_particle.plist");
					_bloom7particle_tail->setPosition(bloom7->getContentSize().width / 5, 42);
					_bloom7particle_tail->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
					sprite->addChild(_bloom7particle_tail);
					_bloom7particle_tail->setVisible(false);

                    _playerTwoWin = sprite;
                    _platformsCollision.pushBack(sprite);
                    break;
                }
                case 3:{ /// rangoli/kolam
                    auto sprite = Sprite::create("images/3.png");
                    sprite->setPosition(j*PIXEL,i*PIXEL);
					_playerReSpawn = Vec2(j*PIXEL, i*PIXEL + sprite->getContentSize().height / 2);
                    platform->addChild(sprite,10);
                    auto s = Sprite::create("images/0_bottom.png");
                    s->setPosition(s->getContentSize().width/2,-1*s->getContentSize().height/2);
                    sprite->addChild(s);
                    _platformsCollision.pushBack(sprite);
                    _idolCollision.pushBack(sprite);
                    break;
                }
                case 9:{ /// player end point 
                    auto sprite = Sprite::create("images/9.png");
                    sprite->setPosition(j*PIXEL,i*PIXEL);
                    platform->addChild(sprite,10);
                    auto s = Sprite::create("images/0_bottom.png");
                    s->setPosition(s->getContentSize().width/2,-1*s->getContentSize().height/2);
                    sprite->addChild(s);

					bloom9 = Sprite::create("images/end_bloom.png");
					bloom9->setPosition(bloom9->getContentSize().width / 5, 0);
					bloom9->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
					sprite->addChild(bloom9);
					bloom9->setVisible(false);

					_bloom9particle_sun = cocos2d::ParticleSystemQuad::create("images/bloom_sun_particle.plist");
					_bloom9particle_sun->setPosition(bloom9->getContentSize().width / 5, 42);
					_bloom9particle_sun->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
					sprite->addChild(_bloom9particle_sun);
					_bloom9particle_sun->setVisible(false);

					_bloom9particle_tail = cocos2d::ParticleSystemQuad::create("images/bloom_tail_particle.plist");
					_bloom9particle_tail->setPosition(bloom9->getContentSize().width / 5, 42);
					_bloom9particle_tail->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
					sprite->addChild(_bloom9particle_tail);
					_bloom9particle_tail->setVisible(false);

                    _playerOneWin = sprite;
                    _platformsCollision.pushBack(sprite);
                    break;
                }
                case 8:{ /// player spawn
                    auto sprite = Sprite::create("images/0.png");
                    sprite->setPosition(j*PIXEL,i*PIXEL);
                    platform->addChild(sprite,10);
                    _platformsCollision.pushBack(sprite);
                    
                    auto s = Sprite::create("images/0_bottom.png");
                    s->setPosition(s->getContentSize().width/2,-1*s->getContentSize().height/2);
                    sprite->addChild(s,2);
                    
                    //reusing variable
                    sprite = Sprite::create("images/player_1.png");
                    _playerOneSpawn = Vec2(j*PIXEL,i*PIXEL + sprite->getContentSize().height/2);
                    sprite->setPosition(_playerOneSpawn);
                    platform->addChild(sprite,1024);
                    _playerOne = sprite;
                    _playerOne->setUserData((void*)"alive");
                    
                    auto animationWalking = Animation::create();
                    animationWalking->setDelayPerUnit(0.1);
                    animationWalking->addSpriteFrameWithFile("images/player_1_walk_0.png");
                    animationWalking->addSpriteFrameWithFile("images/player_1_walk_1.png");
                    animationWalking->addSpriteFrameWithFile("images/player_1_walk_2.png");
                    animationWalking->addSpriteFrameWithFile("images/player_1_walk_3.png");
                    animationWalking->addSpriteFrameWithFile("images/player_1_walk_4.png");
                    animationWalking->addSpriteFrameWithFile("images/player_1_walk_5.png");
                    auto animationWalk = Animate::create(animationWalking);
                    _playerOne->runAction(RepeatForever::create(animationWalk));
                    break;
                }
                case 6:{ /// player spawn
                    auto sprite = Sprite::create("images/0.png");
                    sprite->setPosition(j*PIXEL,i*PIXEL );
                    platform->addChild(sprite,10);
                    _platformsCollision.pushBack(sprite);
                    auto s = Sprite::create("images/0_bottom.png");
                    s->setPosition(s->getContentSize().width/2,-1*s->getContentSize().height/2);
                    sprite->addChild(s,2);
                    
                    //reusing variable
                    sprite = Sprite::create("images/player_2.png");
                    _playerTwoSpawn = Vec2(j*PIXEL,i*PIXEL + sprite->getContentSize().height/2);
                    sprite->setPosition(_playerTwoSpawn);
                    platform->addChild(sprite,1024);
                    _playerTwo = sprite;
                    _playerTwo->setUserData((void*)"alive");
                    
                    
                    auto animationWalking = Animation::create();
                    animationWalking->setDelayPerUnit(0.1);
                    animationWalking->addSpriteFrameWithFile("images/player_2_walk_0.png");
                    animationWalking->addSpriteFrameWithFile("images/player_2_walk_1.png");
                    animationWalking->addSpriteFrameWithFile("images/player_2_walk_2.png");
                    animationWalking->addSpriteFrameWithFile("images/player_2_walk_3.png");
                    animationWalking->addSpriteFrameWithFile("images/player_2_walk_4.png");
                    animationWalking->addSpriteFrameWithFile("images/player_2_walk_5.png");
                    auto animationWalk = Animate::create(animationWalking);
                    _playerTwo->runAction(RepeatForever::create(animationWalk));
                    
                    break;
                }
                case 2:{ /// obstacle
                    std::string str;
                    int toss = RandomHelper::random_int(0, 1);
                    if(toss)
                        str = "blade";
                    else
                        str = "spike";
                    auto sprite = Sprite::create(StringUtils::format("images/%s_base.png",str.c_str()).c_str());
                    sprite->setPosition(j*PIXEL,i*PIXEL);
                    platform->addChild(sprite,10);
					_platformsCollision.pushBack(sprite);
                    _obstacleCollision.pushBack(sprite);
                    sprite->setUserData((void*)"off");
                    
                    auto on = CallFunc::create([this,sprite](){
                        sprite->setUserData((void*)"on");
                    });
                    
                    auto off = CallFunc::create([this,sprite](){
                        sprite->setUserData((void*)"off");
                    });
                    
                    auto randomTime = RandomHelper::random_real(2.0f, 5.0f);
                    
                    auto animation0 = Animation::create();
                    animation0->setDelayPerUnit(0.1);
                    animation0->addSpriteFrameWithFile(StringUtils::format("images/%s_0.png",str.c_str()).c_str());
                    animation0->addSpriteFrameWithFile(StringUtils::format("images/%s_1.png",str.c_str()).c_str());
                    animation0->addSpriteFrameWithFile(StringUtils::format("images/%s_2.png",str.c_str()).c_str());
                    auto animate0 = Animate::create(animation0);
                    
                    
                    auto animation1 = Animation::create();
                    animation1->setDelayPerUnit(0.1);
                    animation1->addSpriteFrameWithFile(StringUtils::format("images/%s_2.png",str.c_str()).c_str());
                    animation1->addSpriteFrameWithFile(StringUtils::format("images/%s_1.png",str.c_str()).c_str());
                    animation1->addSpriteFrameWithFile(StringUtils::format("images/%s_0.png",str.c_str()).c_str());
                    animation1->addSpriteFrameWithFile(StringUtils::format("images/%s_base.png",str.c_str()).c_str());
                    auto animate1 = Animate::create(animation1);
                    
                    sprite->runAction(RepeatForever::create(Sequence::create(DelayTime::create(randomTime),animate0,on,DelayTime::create(randomTime),off,animate1, NULL)));
                    
                    
                    auto s = Sprite::create("images/0_bottom.png");
                    s->setPosition(s->getContentSize().width/2,-1*s->getContentSize().height/2);
                    sprite->addChild(s,2);
                    
                    break;
                }
                default:
                    break;
            }
        }
    }
    
    platform->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    platform->setScale((visibleSize.height/platform->getContentSize().height)*0.7);
    platform->setPosition(visibleSize.width/2,visibleSize.height/2);
    
    
    //Initializing and binding
    auto listener = EventListenerKeyboard::create();
    listener->onKeyPressed = CC_CALLBACK_2(GameScene::onKeyPressed, this);
    listener->onKeyReleased = CC_CALLBACK_2(GameScene::onKeyReleased, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    scheduleUpdate();
    return true;
}

void GameScene::update(float dt){
    
    if(_gcHandler->isTwoABXYPressed()[4]){
        Director::getInstance()->replaceScene(GameScene::createScene());
    }
    if(_gcHandler->isOneABXYPressed()[4]){
        Director::getInstance()->replaceScene(GameScene::createScene());
    }
    
    
    if(_gcHandler->oneAxis().x == 1){
        _playerOneMovement.x += 0.5;
    }
    else if(_gcHandler->oneAxis().x == -1){
        _playerOneMovement.x -= 0.5;
    }
    _playerOneMovement.x = clampf(_playerOneMovement.x,-1*PLAYER_MAX, PLAYER_MAX);
    if(_gcHandler->oneAxis().y == 1){
        _playerOneMovement.y -= 0.5;
    }
    else if(_gcHandler->oneAxis().y == -1){
        _playerOneMovement.y += 0.5;
    }
    _playerOneMovement.y = clampf(_playerOneMovement.y,-1*PLAYER_MAX, PLAYER_MAX);
    
    
    
    
    
    //Player 2 Starts Here
    if(_gcHandler->twoAxis().x == 1){
        _playerTwoMovement.x += 0.5;
    }
    else if(_gcHandler->twoAxis().x == -1){
        _playerTwoMovement.x -= 0.5;
    }
    _playerTwoMovement.x = clampf(_playerTwoMovement.x,-1*PLAYER_MAX, PLAYER_MAX);
    if(_gcHandler->twoAxis().y == 1){
        _playerTwoMovement.y -= 0.5;
    }
    else if(_gcHandler->twoAxis().y == -1){
        _playerTwoMovement.y += 0.5;
    }
    _playerTwoMovement.y = clampf(_playerTwoMovement.y,-1*PLAYER_MAX, PLAYER_MAX);
    
    
    
    
    _playerTwoMovement *= 0.9;
    _playerOneMovement *= 0.9;
    
    
    bool flagOne = false;
    bool flagTwo = false;

	/// falling off platforms
    for(auto c : _platformsCollision){
		auto pos1a = _playerOne->getPosition();
		auto pos1b = _playerOne->getPosition();
		pos1a.x -= _playerOne->getBoundingBox().size.width / 9;
		pos1b.x += _playerOne->getBoundingBox().size.width / 9;
		pos1a.y -= _playerOne->getBoundingBox().size.height / 2;
		pos1b.y -= _playerOne->getBoundingBox().size.height / 2;
		if (c->getBoundingBox().containsPoint(pos1a) || c->getBoundingBox().containsPoint(pos1b)) {
            flagOne = true;
        }
        
        auto pos2a = _playerTwo->getPosition();
		auto pos2b = _playerTwo->getPosition();
		pos2a.x -= _playerTwo->getBoundingBox().size.width / 9;
		pos2b.x += _playerTwo->getBoundingBox().size.width / 9;
        pos2a.y -= _playerTwo->getBoundingBox().size.height/2;
		pos2b.y -= _playerTwo->getBoundingBox().size.height/2;
        if(c->getBoundingBox().containsPoint(pos2a) || c->getBoundingBox().containsPoint(pos2b)){
            flagTwo = true;
        }
    }
    
	/// obstacles
    for(auto c : _obstacleCollision){
        auto pos1 = _playerOne->getPosition();
        pos1.y -= _playerOne->getBoundingBox().size.height/2;
        if(c->getBoundingBox().containsPoint(pos1)){
            auto val = static_cast<const char*>(c->getUserData());
			log("checking ob1 %s", val);
            if( strcmp(val, "on") == 0)
                flagOne = false;

        }
        
        auto pos2 = _playerTwo->getPosition();
        pos2.y -= _playerTwo->getBoundingBox().size.height/2;
        if(c->getBoundingBox().containsPoint(pos2)){
            auto val = static_cast<const char*>(c->getUserData());
			log("checking ob2 %s", val);
            if(strcmp(val, "on") == 0)
                flagTwo = false;
        }
    }
    
	/// idol worship tile
    for(auto c : _idolCollision){
        auto pos1 = _playerOne->getPosition();
        pos1.y -= _playerOne->getBoundingBox().size.height/2;
        if(c->getBoundingBox().containsPoint(pos1)){
            log("-> %d %d %d %d %d",_gcHandler->last5One()[0],_gcHandler->last5One()[1],_gcHandler->last5One()[2],_gcHandler->last5One()[3],_gcHandler->last5One()[4]);
            log("--> %d %d %d %d %d",_playerOnePatternTest[0],_playerOnePatternTest[1],_playerOnePatternTest[2],_playerOnePatternTest[3],_playerOnePatternTest[4]);
            int flag = true;
            for(int i = 0;i < 5;i++){
                if(_gcHandler->last5One()[i] != _playerOnePatternTest[i]){
                    flag = false;
                }
            }
            if(flag){
                _playerOnePattern->setString("go! go! go!");
				bloom9->setVisible(true);
				_bloom9particle_sun->setVisible(true);
				_bloom9particle_tail->setVisible(true);
				_playerOneSpawn = _playerReSpawn;
            }
        }
        auto pos2 = _playerTwo->getPosition();
        pos2.y -= _playerTwo->getBoundingBox().size.height/2;
        if(c->getBoundingBox().containsPoint(pos2)){
            int flag = true;
            for(int i = 0;i < 5;i++){
                if(_gcHandler->last5Two()[i] != _playerTwoPatternTest[i]){
                    flag = false;
                }
            }
            if(flag){
                _playerTwoPattern->setString("go! go! go!");
				bloom7->setVisible(true);
				_bloom7particle_sun->setVisible(true);
				_bloom7particle_tail->setVisible(true);
				_playerTwoSpawn = _playerReSpawn;
            }
        }
        
    }
    
    
    /// respawn player 1 or move
    auto temp1 = static_cast<const char*>(_playerOne->getUserData());
    if(!flagOne  & strcmp(temp1, "died")){
        
        auto cf = CallFunc::create([this](){
            _playerOne->setScale(1);
            _playerOne->setPosition(_playerOneSpawn);
            _playerOne->setOpacity(255);
            _playerOne->setUserData((void*)"alive");
            _playerOne->setRotation(0);
        });
        _gcHandler->vibrate1();
        _playerOne->setUserData((void*)"died");
        _playerOne->runAction(Sequence::create(Spawn::create(FadeOut::create(1),RotateBy::create(1, 360),ScaleTo::create(1, 0),NULL),DelayTime::create(1),cf,NULL));
    }
    else{
        
        _playerOne->setPosition(_playerOne->getPosition() + _playerOneMovement);
        
    }
    
	/// respawn player 2 or move
    auto temp2 = static_cast<const char*>(_playerTwo->getUserData());
    if(!flagTwo && strcmp(temp2, "died")){
        
        auto cf = CallFunc::create([this](){
            _playerTwo->setScale(1);
            _playerTwo->setPosition(_playerTwoSpawn);
            _playerTwo->setOpacity(255);
            _playerTwo->setUserData((void*)"alive");
            _playerTwo->setRotation(0);
        });
        _gcHandler->vibrate2();
        _playerTwo->setUserData((void*)"died");
        _playerTwo->runAction(Sequence::create(Spawn::create(FadeOut::create(1),RotateBy::create(1, 360),ScaleTo::create(1, 0),NULL),DelayTime::create(1),cf,NULL));
    }
    else{
        _playerTwo->setPosition(_playerTwo->getPosition() + _playerTwoMovement);
    }

	/// player win tiles
    auto vs = Director::getInstance()->getVisibleSize();
    if(_playerOneWin->getBoundingBox().containsPoint(_playerOne->getPosition()) && bloom9->isVisible()){
        auto lbl = Label::createWithTTF("Player 1 Wins", "fonts/Phosphate-Inline.ttf", 72);
        lbl->setPosition(vs.width/2,vs.height/2);
        addChild(lbl,2048);
        unscheduleUpdate();

		restartGame();
    }
    else if(_playerTwoWin->getBoundingBox().containsPoint(_playerTwo->getPosition()) && bloom7->isVisible()){
        auto lbl = Label::createWithTTF("Player 2 Wins", "fonts/Phosphate-Inline.ttf", 72);
        lbl->setPosition(vs.width/2,vs.height/2);
        addChild(lbl,2048);
        unscheduleUpdate();

		restartGame();
    }
    
    
}

void GameScene::restartGame() {

	auto delayTime = DelayTime::create(3);
	auto lbl = Label::createWithTTF("Restarting in 3..2..1", "fonts/Phosphate-Inline.ttf",72);
	auto vs = Director::getInstance()->getVisibleSize();
	lbl->setPosition(vs.width / 2, vs.height*0.30);
	addChild(lbl, 3076);
	auto cf = CallFunc::create([this]() {
		Director::getInstance()->replaceScene(GameScene::createScene());
	});

	runAction(Sequence::create(delayTime, cf, NULL));
}

void GameScene::mapGenerator(){
    for (int i = 0; i < MAP_HEIGHT; i++) {
        for(int j = 0;j< MAP_WIDTH;j++){
            _map[i][j] = 1;
        }
    }
    int cost = 0;
    int lastCost = 2048;
    int divisor = 4;
    int wid = MAP_WIDTH-1;
    int high = MAP_HEIGHT-1;
    Vec2 start = Vec2(RandomHelper::random_int(0, wid/divisor),RandomHelper::random_int(0, high/divisor));
    Vec2 newPos = start;
    _map[(int)start.y][(int)start.x] = 8; //start player 1
    
    Vec2 end = Vec2(RandomHelper::random_int(wid - (wid/divisor), wid),RandomHelper::random_int(high - (high/divisor), high));
    _map[(int)end.y][(int)end.x] = 9; // start player 2
    
    
    log("%f %f s",start.x,start.y);
    
    log("%f %f e",end.x,end.y);
    
    
    
    int failedTime = 0;
    while(true){
        bool failSafe = false;
        int dir = RandomHelper::random_int(0, 3);
        Vec2 temp;
        if(failedTime >= 10){
            if(newPos.y == MAP_WIDTH)
                dir = 2;
            else
                dir = 1;
            failedTime = 0;
            failSafe = true;
        }
        if(newPos == end)
            break;
        switch (dir) {
            case 0: //Left
                temp.x = newPos.x -1;
                temp.y = newPos.y;
                if(temp.x < 0)
                    continue;
                cost = std::abs(end.x - temp.x) + std::abs(end.y - temp.y);
                break;
            case 1: //Right
                temp.x = newPos.x + 1;
                temp.y = newPos.y;
                if(temp.x >= MAP_WIDTH)
                    continue;
                cost = std::abs(end.x - temp.x) + std::abs(end.y - temp.y);
                break;
            case 2: //Up
                temp.x = newPos.x;
                temp.y = newPos.y + 1;
                if(temp.y >= MAP_HEIGHT)
                    continue;
                cost = std::abs(end.x - temp.x) + std::abs(end.y - temp.y);
                break;
            case 3: //Down
                temp.x = newPos.x;
                temp.y = newPos.y + 1;
                if(temp.y < 0)
                    continue;
                cost = std::abs(end.x - temp.x) + std::abs(end.y - temp.y);
                break;
            default:
                break;
        }
        log("cost %d lastCost %d",cost,lastCost);
        
        if(cost + 1 <= lastCost || cost == 0){
            auto val = _map[(int)temp.y][(int)temp.x];
            if((val == 0 || val == 8) && failSafe == false){
                failedTime++;
            }
            else{
                newPos = temp;
                lastCost = cost;
                log("%f %f p",newPos.x,newPos.y);
                if(val != 9)
                    _map[(int)newPos.y][(int)newPos.x] = 0; //Path
            }
        }
    }
    
    for (int i = 0; i < MAP_HEIGHT; i++) {
        for(int j = 0;j< MAP_WIDTH;j++){
            auto val = _map[i][j];
            if(val == 8){
                _map[MAP_HEIGHT-1-i][j] = 6;
            }
            else if(val == 9){
                _map[MAP_HEIGHT-1-i][j] = 7;
            }
            else if(val == 0){
                _map[MAP_HEIGHT-1-i][j] = 0;
                if(MAP_HEIGHT-1-i == i){
                    _map[i][j] = 3;
                }
            }
        }
    }
    
    int noOfPath = 0;
    for (int i = 0; i < MAP_HEIGHT; i++) {
        for(int j = 0;j< MAP_WIDTH;j++){
            auto val = _map[i][j];
            if(val == 0){
                noOfPath++;
            }
        }
    }
    
    //int frequencyOfSpikes = round(noOfPath/10);
	int frequencyOfSpikes = 5;
    
    int count = 0;
    for (int i = 0; i < MAP_HEIGHT; i++) {
        for(int j = 0;j< MAP_WIDTH;j++){
            auto val = _map[i][j];
            if(val == 0){
                count++;
                if(count % frequencyOfSpikes == 0){
                    //count = 0;
                    _map[i][j] = 2;
                }
            }
        }
    }
    //LOG
    for (int i = 0; i < MAP_HEIGHT; i++) {
        std::stringstream ss;
        for(int j = 0;j< MAP_WIDTH;j++){
            ss << StringUtils::format("%d", _map[i][j] );
        }
        log("%s",ss.str().c_str());
        log("");
    }
}


void GameScene::menuCloseCallback(Ref* pSender)
{
    Director::getInstance()->end();
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}




// Implementation of the keyboard event callback function prototype
void GameScene::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
    log("Key with keycode %d pressed", keyCode);
    if(keyCode == EventKeyboard::KeyCode::KEY_SPACE){
        Director::getInstance()->replaceScene(GameScene::createScene());
    }

	if (keyCode == EventKeyboard::KeyCode::KEY_ESCAPE) {
		Director::getInstance()->end();
	}
}


void GameScene::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
    
}

